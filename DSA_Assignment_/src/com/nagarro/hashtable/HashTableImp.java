package com.nagarro.hashtable;
	import java.util.Arrays;

	public class HashTableImp {
	private Item keys = new Item();

	private Item elements = new Item();

	public HashTableImp() {
	}

	public void setSize(int newSize) {
	keys.setSize(newSize);
	elements.setSize(newSize);
	}

	public void setGrow(int growBy) {
	keys.setGrow(growBy);
	elements.setGrow(growBy);
	}

	public synchronized void put(String key, Object element) {
	try {
	if (containsKey(key)) {
	elements.add(keys.location(key), element);
	} else {
	keys.add(key);
	elements.add(element);
	}
	} catch (Exception nsoe) {
	}
	}

	public synchronized void remove(String key) {
	try {
	if (containsKey(key)) {
	elements.remove(keys.location(key));
	elements.remove(elements.location(key));
	}
	} catch (Exception nsoe) {
	}
	}

	public int size() {
	return keys.getCurrentSize();
	}

	public boolean contains(Object element) {
	try {
	elements.location(element);
	return (true);
	} catch (Exception noSuchObject) {
	return false;
	}
	}

	public boolean containsKey(String key) {
	try {
	keys.location(key);
	} catch (Exception noSuchObject) {
	return false;
	}
	return (true);
	}

	public Object get(String key) {
	try {
	if (containsKey(key))
	return (elements.get(keys.location(key)));
	} catch (Exception nsoe) {
	}
	return null;
	}

	public void printElement() {
	String key;
	String value = null;
	String[] st=keys.printElement();
	String[] st2=elements.printElement();

	for (int i=0;i<st.length;i++) {
	key=st[i];
	value=st2[i];
	System.out.println("Key values are " +key+"----"+value);
	}
	}

	}

	class Item {
	private int current = 0;

	private int size = 3;

	private int grow = 2;

	private int place = 0;

	private Object[] elements = null;

	private Object[] tmpElements = null;

	public Item() {
	init();
	}

	public Item(int size) {
	setSize(size);
	init();
	}

	public Item(int size, int grow) {
	setSize(size);
	setGrow(grow);
	init();
	}

	private void init() {
	elements = new Object[size];
	}

	public Object nextElement() throws Exception {
	if (elements[place] != null && place != current) {
	place++;
	return elements[place - 1];
	} else {
	place = 0;
	throw new java.util.NoSuchElementException();
	}
	}


	public void setSize(int size) {
	this.size = size;
	}

	public int getCurrentSize() {
	return current;
	}

	public void rehash() {
	tmpElements = new Object[size];
	int count = 0;
	for (int x = 0; x < elements.length; x++) {
	if (elements[x] != null) {
	tmpElements[count] = elements[x];
	count++;
	}
	}
	elements = (Object[]) tmpElements.clone();
	tmpElements = null;
	current = count;
	}

	public void setGrow(int grow) {
	this.grow = grow;
	}

	public void grow() {
	size = size += (size / grow);
	rehash();
	}

	public void add(Object o) {
	if (current == elements.length)
	grow();

	try {
	elements[current] = o;
	current++;
	} catch (java.lang.ArrayStoreException ase) {
	}
	}

	public void add(int location, Object o) {
	try {
	elements[location] = o;
	} catch (java.lang.ArrayStoreException ase) {
	}
	}

	public void remove(int location) {
	elements[location] = null;
	}

	public int location(Object o) throws Exception {
	int loc = -1;
	for (int x = 0; x < elements.length; x++) {
	if ((elements[x] != null && elements[x] == o)
	|| (elements[x] != null && elements[x].equals(o))) {
	loc = x;
	break;
	}
	}
	if (loc == -1)
	throw new Exception();
	return (loc);
	}

	public Object get(int location) {
	return elements[location];
	}

	public String[] printElement() {
	String[] st = new String[3];
	int val = 0;
	for (Object i : elements) {

	st[val] = (String) i;
	val++;
	}
	return st;

	}

	}


